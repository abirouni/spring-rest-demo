package com.springRest.demo.controller;

public class StudentErrorNotFoundException extends RuntimeException {

	public StudentErrorNotFoundException(String message) {
		super(message);
	}
	
	
}
