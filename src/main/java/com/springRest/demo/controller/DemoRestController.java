package com.springRest.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import com.springRest.demo.entity.Student;

@RestController
@RequestMapping("/api")
public class DemoRestController {

	// refactor our code to only load data once
	// will be run once immediately after the bean's initialization.
	private List<Student> students;

	@PostConstruct
	public void loadData() {

		students = new ArrayList<Student>();
		students.add(new Student("sheldon", "farah"));
		students.add(new Student("marry", "kazanouva"));
		students.add(new Student("suzan", "bang"));
	}

	// add code for the "/hello" endpoint
	@GetMapping("/hello")
	public String showHello() {
		return "Hello world !";
	}

	// define endpoint for "/test/student" -return list of student
	@GetMapping("/student")
	public List<Student> getStudent() {

		return students;
	}

	// define endpoint for "/student/{studentId}" -return student at index
	@GetMapping("/student/{studentId}")
	public Student getStudentById(@PathVariable int studentId) {

		if ((studentId > students.size()) || (studentId < 0)) {
			throw new StudentErrorNotFoundException("Student id is not found " + studentId);
		}
		return students.get(studentId);
	}

}
